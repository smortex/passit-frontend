import { platformNativeScriptDynamic, AppOptions } from "@nativescript/angular";

import { AppModule } from "./app/app.module";

const options: AppOptions = {};

platformNativeScriptDynamic(options).bootstrapModule(AppModule);
