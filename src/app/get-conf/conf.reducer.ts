import { Actions, ConfTypes } from "./conf.actions";

export interface IConfState {
  isPrivateOrgMode: boolean;
  environment: string | null;
  /** Is operating in web extension popup */
  isPopup: boolean;
  timestamp: string | null;
  ravenDsn: string | null;
  billingEnabled: boolean;
}

export const initial: IConfState = {
  isPrivateOrgMode: false,
  environment: null,
  isPopup: false,
  ravenDsn: null,
  timestamp: null,
  billingEnabled: false,
};

export function reducer(state = initial, action: Actions): IConfState {
  switch (action.type) {
    case ConfTypes.SET_CONF: {
      return {
        ...state,
        isPrivateOrgMode: action.payload.IS_PRIVATE_ORG_MODE,
        environment: action.payload.ENVIRONMENT,
        ravenDsn: action.payload.SENTRY_DSN,
        billingEnabled: action.payload.BILLING_ENABLED,
        timestamp: new Date().toString(),
      };
    }

    case ConfTypes.SET_IS_POPUP: {
      return {
        ...state,
        isPopup: true,
      };
    }

    default: {
      return state;
    }
  }
}

export const getIsPrivateOrgMode = (state: IConfState) =>
  state.isPrivateOrgMode;
export const getTimestamp = (state: IConfState) => state.timestamp;
export const getIsPopup = (state: IConfState) => state.isPopup;
export const getRavenDsn = (state: IConfState) => state.ravenDsn;
export const getBillingEnabled = (state: IConfState) => state.billingEnabled;
