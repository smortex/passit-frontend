import { Component, OnInit } from "@angular/core";
import { select, Store } from "@ngrx/store";
import { IState } from "../app.reducers";
import { ResetFormDirective } from "../form";
import {
  createOrganization,
  getOrganizationList,
  hideCreateOrganization,
  setActiveOrganization,
  showCreateOrganization,
} from "./organizations.actions";
import {
  selectActiveOrganization,
  selectActiveOrganizationID,
  selectEditOrganizationForm,
  selectNewOrganizationForm,
  selectOrganizationLoadingComplete,
  selectOrganizationOptions,
  selectOrganizations,
  selectOrganizationShowCreate,
  selectOrganizationShowOnboarding,
} from "./organizations.selectors";

@Component({
  template: `<app-organizations
    [newForm]="newForm$ | async"
    [editForm]="editForm$ | async"
    [loadingComplete]="loadingComplete$ | async"
    [organizations]="organizations$ | async"
    [activeOrganization]="activeOrganization$ | async"
    [activeOrganizationID]="activeOrganizationID$ | async"
    [organizationOptions]="organizationOptions$ | async"
    [showCreate]="showCreate$ | async"
    [showOnboarding]="showOnboarding$ | async"
    (addNew)="showCreate()"
    (cancelNew)="hideCreate()"
    (createOrganization)="createOrganization()"
    (setActiveOrganization)="setActiveOrganization($event)"
  ></app-organizations>`,
})
export class OrganizationsContainer
  extends ResetFormDirective
  implements OnInit {
  newForm$ = this.store.pipe(select(selectNewOrganizationForm));
  editForm$ = this.store.pipe(select(selectEditOrganizationForm));
  loadingComplete$ = this.store.pipe(select(selectOrganizationLoadingComplete));
  organizations$ = this.store.pipe(select(selectOrganizations));
  organizationOptions$ = this.store.pipe(select(selectOrganizationOptions));
  showCreate$ = this.store.pipe(select(selectOrganizationShowCreate));
  showOnboarding$ = this.store.pipe(select(selectOrganizationShowOnboarding));
  activeOrganizationID$ = this.store.pipe(select(selectActiveOrganizationID));
  activeOrganization$ = this.store.pipe(select(selectActiveOrganization));

  constructor(store: Store<IState>) {
    super(store);
  }

  ngOnInit() {
    super.ngOnInit();
    this.store.dispatch(getOrganizationList());
  }

  showCreate() {
    this.store.dispatch(showCreateOrganization());
  }

  hideCreate() {
    this.store.dispatch(hideCreateOrganization());
  }

  createOrganization() {
    this.store.dispatch(createOrganization());
  }

  setActiveOrganization(id: number) {
    this.store.dispatch(setActiveOrganization({ organizationID: id }));
  }
}
