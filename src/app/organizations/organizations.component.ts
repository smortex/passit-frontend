import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from "@angular/core";
import { IOption } from "ng-select";
import { FormGroupState } from "ngrx-forms";
import { Organization } from "./organizations.interfaces";
import {
  EditOrganizationFormValue,
  NewOrganizationFormValue,
} from "./organizations.reducers";

@Component({
  selector: "app-organizations",
  styleUrls: ["../list/list.component.scss"],
  templateUrl: "organizations.component.html",
})
export class OrganizationsComponent {
  @Input() newForm: FormGroupState<NewOrganizationFormValue>;
  @Input() editForm: FormGroupState<EditOrganizationFormValue>;
  @Input() loadingComplete: boolean;
  @Input() organizations: Organization[];
  @Input() activeOrganization: Organization;
  @Input() organizationOptions: IOption[];
  @Input() showCreate: boolean;
  @Input() showOnboarding: boolean;
  @Input() set activeOrganizationID(value: number | null) {
    if (value && this.activeOrganizationSelect) {
      this.activeOrganizationSelect.select(value.toString());
    }
  }
  @Output() addNew = new EventEmitter();
  @Output() cancelNew = new EventEmitter();
  @Output() createOrganization = new EventEmitter();
  @Output() setActiveOrganization = new EventEmitter<number>();

  @ViewChild("activeOrganizationSelect") activeOrganizationSelect: any;

  selectActiveOrganization(option: IOption) {
    this.setActiveOrganization.emit(parseInt(option.value, 10));
  }
}
