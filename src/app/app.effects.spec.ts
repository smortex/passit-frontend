import { TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { provideMockActions } from "@ngrx/effects/testing";
import { Observable, ReplaySubject } from "rxjs";

import { StoreModule, combineReducers } from "@ngrx/store";

import * as fromRoot from "./app.reducers";
import { AppEffects } from "./app.effects";
import { UserService } from "./account/user";
import { LogoutSuccessAction } from "./account/account.actions";

describe("App Effects", () => {
  let effects: AppEffects;
  let actions: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          feature: combineReducers(fromRoot.reducers)
        })
      ],
      providers: [
        AppEffects,
        provideMockActions(() => actions),
        {
          provide: UserService,
          useValue: jasmine.createSpyObj("UserService", ["login"])
        }
      ]
    });

    effects = TestBed.inject(AppEffects);
  });

  it("logout should remove auth localstorage", () => {
    localStorage.setItem("auth", "fake");

    actions = new ReplaySubject(1);
    (actions as any).next(new LogoutSuccessAction());

    effects.logout$.subscribe(() => {
      expect(localStorage.getItem("auth")).toBe(null);
    });
  });
});
