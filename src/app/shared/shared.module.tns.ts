import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptRouterModule, NativeScriptCommonModule } from "@nativescript/angular";
import { NgrxFormsModule } from "ngrx-forms";

import { ButtonComponent } from "./button/button.component.tns";
import { TextFieldComponent } from "./text-field/text-field.component";
import { HeadingComponent } from "./heading/heading.component";
import { SearchComponent } from "./search/search.component";
import { NonFieldMessagesComponent } from "./non-field-messages/non-field-messages.component";
import { DirectivesModule } from "../directives";
import { CheckboxComponent } from "./checkbox/checkbox.component";
import { AsideLinkComponent } from "./aside-link/aside-link.component";
import { TextLinkComponent } from "./text-link/text-link.component";
import { NsCheckboxComponent } from "./ns-checkbox/ns-checkbox.component";
import { ServerSelectComponent } from "./server-select/server-select.component";
import { AccountFrameComponent } from "./account-frame/account-frame.component";
import { BulletListComponent } from "./bullet-list/bullet-list.component.tns";
import { MultiselectListComponent } from "./multiselect/multiselect-list/multiselect-list.component";
import { MultiselectComponent } from "./multiselect/multiselect.component";
import { BadgeComponent } from "./multiselect/badge/badge.component";
import { MultiselectAddComponent } from "./multiselect/multiselect-add/multiselect-add.component";

export const COMPONENTS = [
  ButtonComponent,
  TextFieldComponent,
  HeadingComponent,
  CheckboxComponent,
  AsideLinkComponent,
  SearchComponent,
  NonFieldMessagesComponent,
  NsCheckboxComponent,
  TextLinkComponent,
  ServerSelectComponent,
  AccountFrameComponent,
  BulletListComponent,
  MultiselectListComponent,
  MultiselectComponent,
  BadgeComponent,
  MultiselectAddComponent
];

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NgrxFormsModule,
    DirectivesModule,
    NativeScriptRouterModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule {}
