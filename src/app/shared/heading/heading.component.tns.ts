import { ChangeDetectionStrategy, Component, Input } from "@angular/core";

@Component({
  selector: "app-heading",
  template: `
    <Label [text]="text" textWrap="true" class="app-heading"></Label>
  `,
  styles: [
    `
      .app-heading {
        color: #413741;
        font-size: 24;
        font-weight: bold;
      }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeadingComponent {
  @Input() text: string;

  constructor() {}
}
