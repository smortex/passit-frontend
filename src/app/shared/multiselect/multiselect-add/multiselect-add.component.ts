import {
  Component,
  EventEmitter,
  Input,
  Output,
  ChangeDetectionStrategy,
  HostListener
} from "@angular/core";
import { AbstractControlState } from "ngrx-forms";
import { IMultiselectList } from "../multiselect-list/multiselect-list.component";

@Component({
  selector: "app-multiselect-add",
  templateUrl: "./multiselect-add.component.html",
  styleUrls: ["./multiselect-add.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiselectAddComponent {
  _searchField: AbstractControlState<string>;
  @Input() set ngrxFormControl(value: AbstractControlState<string>) {
    /**
     * This catches a thing in the keyboard nav scheme where
     * _keyboardActiveListItem would reset before you could use it
     */
    if (this._searchField && this._searchField.value !== value.value) {
      this.resetKeyboardListActiveItem();
    }
    /** Prevents keyboard nav UI from showing with mouse usage */
    if (this.keyControls) {
      this.setKeyboardListActiveItem();
    }
    this._searchField = value;
    this.validateSearchField(this._searchField.value);
  }

  /**
   * Modal mode means that it shows the whole list and checkboxes so the user
   * can keep context while in the modal
   */
  _modalMode: boolean;
  @Input() set modalMode(value: boolean) {
    this._modalMode = value;
    this.filterListItems();
  }

  _listItems: IMultiselectList[] = [];
  _visibleListItems: IMultiselectList[] = [];
  /**
   * As far as I can tell, this fires on init, when there's a match in
   * public mode, and when something is added to the list
   */
  @Input() set listItems(items: IMultiselectList[]) {
    this._listItems = items;
    this.filterListItems();
  }

  /**
   * List item that was just added. When you addListItem, it'll populate
   * this. If you click into the search box again, it'll reset this.
   */
  _justAdded = "";
  /**
   * Allows a distinction between items that aren't checked and items that
   * aren't visible because they don't match the search field
   */
  _unselectedListItemsCount: number;
  /**
   * Correlates with the ngFor index for _visibleListItems
   */
  _keyboardActiveListItem: number | null = null;
  @Input() label: string;
  /**
   * This could probably go internal
   */
  @Input() totalListItemsCount: number;
  @Input() keyControls: boolean;
  @Output() toggleKeyControls = new EventEmitter();
  @Output() addListItem = new EventEmitter<number>();
  @Output() removeListItem = new EventEmitter<number>();
  @Output() closeModalMode = new EventEmitter();
  /**
   * Helper boolean for search field validation
   */
  _validSearchField = true;
  @Input() searchFieldValidationPattern: RegExp;
  @Input() invalidSearchFieldText: string;
  @Input() noSearchFieldMatchText: string;
  @Input() noListItemsText: string;
  @Input() fewListItemsText: string;
  @Input() allListItemsSelectedText: string;
  @Input() justAddedText: string;
  _justAddedText: string;

  validateSearchField(searchFieldValue: string) {
    if (this.searchFieldValidationPattern) {
      const searchFieldMatchesPattern = !!searchFieldValue.match(
        this.searchFieldValidationPattern
      );
      this._validSearchField =
        searchFieldMatchesPattern && searchFieldValue !== "";
    }
  }

  addToList = (listItem: IMultiselectList) => {
    if (listItem.isSelected) {
      this.removeListItem.emit(listItem.value);
    } else {
      this.addListItem.emit(listItem.value);
      this._justAdded = listItem.name;
      this.buildJustAddedText();
    }
  };

  buildJustAddedText = () => {
    if (!!this.justAddedText) {
      // Arbitrary marker I created for the sole purpose of using here
      if (this.justAddedText.includes("|||")) {
        this._justAddedText = this.justAddedText.replace(
          "|||",
          this._justAdded
        );
      } else {
        this._justAddedText = this.justAddedText;
      }
    } else {
      this._justAddedText = "";
    }
  };

  searchFieldFocus = () => {
    this._justAdded = "";
    this.toggleKeyControls.emit();
    this.setKeyboardListActiveItem();
  };

  searchFieldBlur = () => {
    this.toggleKeyControls.emit();
    this.resetKeyboardListActiveItem();
  };

  filterListItems = () => {
    this._visibleListItems = this._listItems;
    if (this._searchField) {
      if (!this._modalMode) {
        this._visibleListItems = this._visibleListItems.filter(
          item => !item.isSelected
        );
        this._unselectedListItemsCount = this._visibleListItems.length;
      }
      if (!!this._searchField.value) {
        this._visibleListItems = this._visibleListItems.filter(
          item => item.name.indexOf(this._searchField.value) > -1
        );
      }
    }
  };

  resetKeyboardListActiveItem = () => {
    this._keyboardActiveListItem = null;
  };

  setKeyboardListActiveItem = (key?: "ArrowDown" | "ArrowUp") => {
    if (this._keyboardActiveListItem === null) {
      if (this._visibleListItems.length > 0) {
        this._keyboardActiveListItem = 0;
      }
      return;
    }
    if (key === "ArrowDown") {
      if (this._keyboardActiveListItem < this._visibleListItems.length - 1) {
        this._keyboardActiveListItem = this._keyboardActiveListItem + 1;
      } else {
        this._keyboardActiveListItem = 0;
      }
      return;
    }
    if (key === "ArrowUp") {
      if (this._keyboardActiveListItem === 0) {
        this._keyboardActiveListItem = this._visibleListItems.length - 1;
      } else {
        this._keyboardActiveListItem = this._keyboardActiveListItem - 1;
      }
      return;
    }
  };

  @HostListener("document:keydown", ["$event"])
  onKeydownHandler(event: any) {
    if (event.key === "ArrowDown" || event.key === "ArrowUp") {
      if (this.keyControls) {
        /**
         * Up/down arrows will take you to the beginning or end of a text field
         * by default; we don't want that. Unless the shift key is on, then we
         * probably don't want to do our arrow nav.
         */
        if (!event.shiftKey) {
          event.preventDefault();
          this.setKeyboardListActiveItem(event.key);
        }
      }
    }
  }

  @HostListener("document:mousedown", ["$event"])
  onMousedownHandler(event: any) {
    // I think this is a safe assumption
    const element = event.target as Element;
    if (element.getAttribute("data-multiselect-list-item")) {
      this.resetKeyboardListActiveItem();
    }
  }

  @HostListener("document:keyup", ["$event"])
  onKeyupHandler(event: any) {
    if (
      event.key === "Enter" &&
      this.keyControls &&
      this._keyboardActiveListItem !== null &&
      this._visibleListItems[this._keyboardActiveListItem]
    ) {
      this.addToList(this._visibleListItems[this._keyboardActiveListItem]);
      this.resetKeyboardListActiveItem();
    }
  }
}
