import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { AbstractControlState, ValidationErrors } from "ngrx-forms";

@Component({
  selector: "app-server-select",
  templateUrl: "./server-select.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServerSelectComponent {
  @Input() showUrlControl: AbstractControlState<boolean>;
  @Input() urlControl: AbstractControlState<string>;
  @Input() formErrors: ValidationErrors;
  /** Needed for NS on submit, not used in web */
  @Output() onSubmit = new EventEmitter();

  constructor() {}
}

