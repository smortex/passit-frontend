import { take } from "rxjs/operators";
import { fakeAsync, inject, TestBed } from "@angular/core/testing";
import { Store, StoreModule, select } from "@ngrx/store";

import * as fromRoot from "./app.reducers";
import * as fromAuth from "./auth.reducer";
import { LoginAction, LoginSuccessAction } from "./app.actions";
import { LogoutSuccessAction } from "./account/account.actions";
import { IAuthStore } from "./account/user";

describe("AccountReducer", () => {
  describe("undefined action", () => {
    it("should return the default state", () => {
      const action = {} as any;

      const result = fromAuth.authReducer(undefined, action);
      expect(result).toEqual(fromAuth.initialState);
    });
  });

  describe("wrong login payload", () => {
    it("should NOT authenticate a user", () => {
      const createAction = new LoginAction();

      const startState = {
        ...fromAuth.initialState,
        login: { ...fromAuth.initialState, form: { email: "test" } }
      };
      const expectedResult = fromAuth.initialState;

      const result = fromAuth.authReducer(startState, createAction);
      expect(result).toEqual(expectedResult);
    });
  });

  describe("LOGIN_SUCCESS", () => {
    it("should add a user set loggedIn to true in auth state", () => {
      const user: IAuthStore = {
        email: "test@example.com",
        privateKey: "fake",
        publicKey: "fake",
        userId: 1,
        userToken: "aaa",
        rememberMe: false,
        optInErrorReporting: false,
        mfaRequired: false
      };
      const createAction = new LoginSuccessAction(user);

      const expectedResult: fromAuth.IAuthState = {
        email: "test@example.com",
        userId: 1,
        privateKey: "fake",
        publicKey: "fake",
        userToken: "aaa",
        url: fromAuth.initialState.url,
        rememberMe: false,
        optInErrorReporting: false,
        forceSetPassword: false,
        mfaRequired: false
      };

      const result = fromAuth.authReducer(fromAuth.initialState, createAction);
      expect(result).toEqual(expectedResult);
      // Mimic slice of global state
      const accountState: fromRoot.IState = {
        ...fromRoot.initialState,
        auth: result
      };
      // selector should say we are now logged in
      expect(fromRoot.getIsLoggedIn(accountState)).toBe(true);
    });
  });

  describe("LOGOUT", () => {
    beforeEach(() => {
      // Bootstrap the logout metareducer
      const metaReducers = [fromRoot.logout];
      TestBed.configureTestingModule({
        imports: [StoreModule.forRoot(fromRoot.reducers, { metaReducers })],
        providers: []
      });
    });

    it("should logout a user", fakeAsync(
      inject([Store], (store: Store<any>) => {
        const isLoggedInSelector = store.pipe(select(fromRoot.getIsLoggedIn));

        // Login first
        const user: IAuthStore = {
          email: "test@example.com",
          privateKey: "fake",
          publicKey: "fake",
          userId: 1,
          userToken: "aaa",
          rememberMe: false,
          optInErrorReporting: false,
          mfaRequired: false
        };
        store.dispatch(new LoginSuccessAction(user));
        isLoggedInSelector
          .pipe(take(1))
          .subscribe(isLoggedIn => expect(isLoggedIn).toBe(true));

        // Now logout
        store.dispatch(new LogoutSuccessAction());
        isLoggedInSelector
          .pipe(take(1))
          .subscribe(isLoggedIn => expect(isLoggedIn).toBe(false));
      })
    ));
  });
});
