import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";

import { ProgressIndicatorComponent } from "./progress-indicator.component";

const COMPONENTS = [ProgressIndicatorComponent];

@NgModule({
  imports: [NativeScriptCommonModule],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  schemas: [NO_ERRORS_SCHEMA]
})
export class ProgressIndicatorModule {}
