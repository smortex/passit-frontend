import { SecretService } from "./secret.service";
import { testSecret } from "./test_data";
import { ISecret, IData, IGroup } from "../../passit_sdk/api.interfaces";
import { TestBed, waitForAsync } from "@angular/core/testing";
import { NgPassitSDK } from "../ngsdk/sdk";

/* tslint:disable:max-line-length */
export class FakeSDK {
  mockSecret = testSecret;
  ready = () => Promise.resolve();

  list_secrets() {
    return new Promise((resolve, reject) => {
      resolve([this.mockSecret]);
    });
  }
}

describe("Secret Service", () => {
  let service: SecretService;
  let sdk: any;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          SecretService,
          provide: NgPassitSDK,
          useValue: jasmine.createSpyObj("NgPassitSDK", [
            "offline_decrypt_secret",
            "ready"
          ])
        }
      ]
    }).compileComponents();
  }));

  it("get secrets and save in store", (done: any) => {
    sdk = new FakeSDK();
    service = new SecretService(sdk);
    service.getSecrets().subscribe(secrets => {
      expect(secrets[0]).toEqual(sdk.mockSecret);
      done();
    });
  });

  it("showOfflineSecret can handle decrypt errors", async () => {
    sdk = TestBed.inject(NgPassitSDK);
    service = new SecretService(sdk);
    const secret: ISecret = testSecret;
    // Simulate a invalid through and then a valid through
    secret.secret_through_set = [
      {
        data: {
          password: "junk"
        },
        group: 9,
        id: 1,
        is_mine: true,
        key_ciphertext: "error!",
        public_key: "bad"
      },
      secret.secret_through_set[0]
    ];
    const group: IGroup = {
      id: 1,
      groupuser_set: [],
      my_key_ciphertext: "a",
      my_private_key_ciphertext: "a",
      name: "test group",
      public_key: "abc"
    };
    const decrypted: IData = {
      password: "hunter2"
    };
    sdk.ready.and.returnValue(Promise.resolve());
    // First sdk decrypt fails, second succeeds
    sdk.offline_decrypt_secret.and.returnValues(
      Promise.reject("error!"),
      Promise.resolve(decrypted)
    );
    const result = await service.showOfflineSecret(secret, [group, group]);
    expect(result.password).toEqual(decrypted.password);
    // It should have been called twice since it errors the first time
    expect(sdk.offline_decrypt_secret.calls.count()).toBe(2);
  });
});
