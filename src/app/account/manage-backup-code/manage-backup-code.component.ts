import {
  Component,
  Input,
  Output,
  ChangeDetectionStrategy,
  EventEmitter,
  OnDestroy,
  ViewChild,
  ElementRef
} from "@angular/core";
import { FormGroupState } from "ngrx-forms";
import { IForm } from "./manage-backup-code.reducer";

@Component({
  selector: "app-manage-backup-code",
  templateUrl: "./manage-backup-code.component.html",
  styleUrls: [
    "./manage-backup-code.component.scss",
    "../account.component.scss",
    "../../../styles/_utility.scss"
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageBackupCodeComponent implements OnDestroy {
  @Input() form: FormGroupState<IForm>;
  @Input() errorMessage: string;
  @Input() hasStarted: boolean;
  @Input() hasFinished: boolean;
  @Input() nonFieldErrors: string[];
  @Input() backupCode: string;

  @Output() newBackupCode = new EventEmitter();
  @Output() getBackupPDF = new EventEmitter();
  @Output() reset = new EventEmitter();
  @Output() toggleShowConfirm = new EventEmitter();

  @ViewChild("passwordInput") passwordInput: ElementRef;

  onSubmit() {
    if (this.form.isValid) {
      this.newBackupCode.emit();
    }
  }

  ngOnDestroy() {
    this.reset.emit();
  }
}
