import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { UserService } from "../user";
import {
  ManageMfaActionTypes,
  EnableMfaSuccess,
  EnableMfaFailure,
  ActivateMfaFailure,
  ActivateMfaSuccess,
  DeactivateMfaSuccess,
  DeactivateMfaFailure
} from "./manage-mfa.actions";
import { exhaustMap, map, catchError, withLatestFrom } from "rxjs/operators";
import { of } from "rxjs";
import { Store, select } from "@ngrx/store";
import { IState } from "../../app.reducers";
import { getEnableMfaForm, getMfaId } from "../account.reducer";

@Injectable()
export class ManageMFAEffects {
  @Effect()
  generateMfa$ = this.actions$.pipe(
    ofType(ManageMfaActionTypes.ENABLE_MFA),
    exhaustMap(action =>
      this.service.generateMfa().pipe(
        map(resp => new EnableMfaSuccess(resp)),
        catchError(resp => of(new EnableMfaFailure(resp)))
      )
    )
  );

  @Effect()
  activateMfa$ = this.actions$.pipe(
    ofType(ManageMfaActionTypes.ACTIVATE_MFA),
    withLatestFrom(
      this.store.pipe(select(getEnableMfaForm)),
      this.store.pipe(select(getMfaId))
    ),
    exhaustMap(([action, form, id]) => {
      if (form.isValid && form.value.verificationCode && id) {
        return this.service.activateMfa(form.value.verificationCode, id).pipe(
          map(resp => new ActivateMfaSuccess()),
          catchError(resp => of(new ActivateMfaFailure()))
        );
      }
      return of(new ActivateMfaFailure());
    })
  );

  @Effect()
  deactivateMfa$ = this.actions$.pipe(
    ofType(ManageMfaActionTypes.DEACTIVATE_MFA),
    exhaustMap(action =>
      this.service.deactivateMfa().pipe(
        map(resp => new DeactivateMfaSuccess()),
        catchError(resp => of(new DeactivateMfaFailure()))
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<IState>,
    private service: UserService
  ) {}
}
