import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "@nativescript/angular";
import { Routes } from "@angular/router";

import { SecretDetailComponent } from "./list/detail.component";
import { SecretNewComponent } from "./list/new.component.tns";
import { LoggedInGuard } from "./guards";
import { routes } from "./app.common";

export const appRoutes: Routes = [
  ...routes,
  {
    path: "list/:id",
    component: SecretDetailComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: "list/new",
    component: SecretNewComponent,
    canActivate: [LoggedInGuard]
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(appRoutes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}
