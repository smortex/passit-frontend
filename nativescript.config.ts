import { NativeScriptConfig } from '@nativescript/core'

export default {
  id: 'com.burkesoftware.Passit',
  appResourcesPath: 'App_Resources',
  android: {
    v8Flags: '--expose_gc',
    markingMode: 'none',
  },
  name: 'migration-ng',
  version: '4.1.0',
  appPath: 'src',
  nsext: '.tns',
  webext: '',
  shared: true,
} as NativeScriptConfig
